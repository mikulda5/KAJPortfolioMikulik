# Webová aplikace pro portfolio a životopis

## Cíl projektu

Cílem tohoto projektu je webová aplikace, která slouží jako online portfolio a životopis pro jednotlivce.

## Popis funkčnosti

- **HTML5:**
    - Validní použití HTML5 doctype
    - Fungující v moderních prohlížečích (Chrome, Firefox, Edge, Opera).
    - Správné použití sémantických značek (section, article, nav, aside, ...)
    - Použití SVG
    - Média - Audio.
    - Formulářové prvky: Validace, typy, placeholder, autofocus.
    - Offline aplikace

- **CSS:**
    - Použití pokročilých pseudotříd a kombinátorů. Vendor prefixy.
    - CSS3 transformace 2D/3D, transitions/animations.
    - Media queries: Stránky fungují i na mobilních zařízeních i jiných .

- **JavaScript:**
    - OOP přístup.
    - Použití JS frameworku či knihovny (jQuery).
    - Použití pokročilých JS API (např. LocalStorage).
    - Zvuk
    - Offline aplikace

- **Portfolio:**
    - Sekce zobrazující projekty a práce jednotlivce, včetně popisů a obrázků.

- **Životopis:**
    - Stránka s informacemi o vzdělání, pracovních zkušenostech a osobních zájmech.

- **Formulář pro kontakt:**
    - Umožňuje návštěvníkům odeslat zprávu přímo prostřednictvím webové stránky.