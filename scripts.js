document.addEventListener("DOMContentLoaded", function() {
    const header = document.querySelector("header");
    const stickyHeader = document.createElement("div");
    stickyHeader.classList.add("sticky-header");
    stickyHeader.innerHTML = `
        <h1 id="sticky-header-title">Tomáš Garrigue Masaryk</h1>
        <nav>
            <ul>
                <li><a href="#about">O mně</a></li>
                <li><a href="#resume">Životopis</a></li>
                <li><a href="#portfolio">Portfolio</a></li>
                <li><a href="#contact">Kontakt</a></li>
            </ul>
        </nav>
    `;
    document.body.appendChild(stickyHeader);

    const headerContent = document.querySelector(".header-content");

    window.addEventListener("scroll", () => {
        if (window.scrollY > header.offsetHeight) {
            stickyHeader.style.display = "flex";
            headerContent.style.opacity = "0";
        } else {
            stickyHeader.style.display = "none";
            headerContent.style.opacity = "1";
        }
    });

    // Hladké rolování mezi sekcemi bez kompenzace pro sticky-header
    $('nav ul li a').on('click', function(event) {
        event.preventDefault();
        const targetId = $(this).attr('href').substring(1);
        const targetElement = document.getElementById(targetId);
        const elementPosition = targetElement.getBoundingClientRect().top + window.scrollY;

        window.scrollTo({
            top: elementPosition,
            behavior: "smooth"
        });
    });

    // Při kliknutí na jméno v sticky headeru se přesměruje na úvodní stránku
    document.getElementById('sticky-header-title').addEventListener('click', function() {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    });

    // Třída FormHandler pro validaci a správu formuláře
    class FormHandler {
        constructor(formId) {
            this.form = document.getElementById(formId);
            this.init();
        }

        init() {
            this.form.addEventListener('submit', this.handleSubmit.bind(this));
        }

        validateEmail(email) {
            const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            return emailPattern.test(email);
        }

        handleSubmit(event) {
            event.preventDefault();
            const name = this.form.querySelector('#name').value;
            const email = this.form.querySelector('#email').value;
            const message = this.form.querySelector('#message').value;

            if (!this.validateEmail(email)) {
                alert('Zadejte platný e-mail.');
                return;
            }

            const formData = {
                name,
                email,
                message
            };
            localStorage.setItem('formSubmission', JSON.stringify(formData));
            alert('Formulář byl úspěšně odeslán.');
            this.form.reset();
        }
    }

    // Iniciace formuláře s ID "contact"
    new FormHandler('contactForm');

    // Registrace Service Worker
    if ('serviceWorker' in navigator && window.location.protocol !== 'file:') {
        navigator.serviceWorker.register('/service-worker.js')
            .then(registration => {
                console.log('Service Worker registered with scope:', registration.scope);
            })
            .catch(error => {
                console.error('Service Worker registration failed:', error);
            });
    }

    // Přehrávání MP3 souboru
    const audio = document.getElementById('background-audio');
    const playIcon = document.getElementById('audio-icon-play');
    const pauseIcon = document.getElementById('audio-icon-pause');
    let isPlaying = false;

    function toggleAudio() {
        if (isPlaying) {
            audio.pause();
            pauseIcon.style.display = 'none';
            playIcon.style.display = 'inline';
        } else {
            audio.play().catch(error => {
                console.error('Audio playback error:', error);
            });
            playIcon.style.display = 'none';
            pauseIcon.style.display = 'inline';
        }
        isPlaying = !isPlaying;
    }

    playIcon.addEventListener('click', toggleAudio);
    pauseIcon.addEventListener('click', toggleAudio);
});
